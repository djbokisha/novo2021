import App from './App.jsx';
import {render, h} from 'preact'
import { html } from 'htm/preact';
 let root;

 function init() {
   root = render(html`<${App }/>`, document.body, root);
 }
 init();

 // Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
 // Learn more: https://www.snowpack.dev/concepts/hot-module-replacement
 if (import.meta.hot) {
  import.meta.hot.accept();
}

